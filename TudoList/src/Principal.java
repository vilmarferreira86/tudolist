import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Principal {

    public static void main(String[] args){
        Menu menu = new Menu();
        System.out.println("Bem vindo ao sistema de TudoList");
        List<String> categorias = new ArrayList<String>();


        boolean continua = true;
        while(continua){
            menu.imprimeMenu();
            String valorDecisao = menu.decisaoUsuario();

            switch (valorDecisao){
                case "1":{
                    System.out.println("Digite o nome da cetegoria sem espaços");
                    Scanner scanNomeCategoria = new Scanner(System.in);
                    String nomeCategoria = scanNomeCategoria.next();

                    categorias.add(nomeCategoria);
                    break;
                }
                case "2":{
                    for(String nome : categorias){
                        System.out.println(nome);
                    }
                    break;
                }
                case "3":{
                    System.out.println("Digite o nome da categoria para remoção");
                    Scanner scanNomeRemocao = new Scanner(System.in);
                    String nomeCategoriaRemocao = scanNomeRemocao.next();

                    categorias.remove(nomeCategoriaRemocao);
                    break;
                }
                case "0":{
                    System.exit(0);
                }
                default:{
                    System.out.println("Opção inválida!");
                    break;
                }
            }
        }

    }


}
